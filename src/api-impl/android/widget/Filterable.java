package android.widget;

public interface Filterable {
	public Filter getFilter();
}
