package android.webkit;

import android.content.Context;

class ValueCallback {}

public class CookieManager {

	public static CookieManager getInstance() {
		// HACK: disable NewPipe's WebView based PoToken generator for now
		if (Context.this_application.getPackageName().equals("org.schabi.newpipe")) {
			throw new RuntimeException("CookieManager not yet fully implemented");
		}
		return new CookieManager();
	}

	public void removeAllCookies(ValueCallback callback) {}

	public void removeSessionCookies(ValueCallback callback) {}

	public void flush() {}
}
